Convert from p12 to jks

``` keytool -importkeystore -srckeystore .\client.p12 -srcstoretype pkcs12 -srcalias 1 -destkeystore client.jks -deststoretype jks -deststorepass private -destalias my-client ```

Good read:
https://erfin-feluzy.medium.com/tutorial-secure-your-api-with-x509-mutual-authentication-with-spring-boot-on-openshift4-416a00a47af8

`openssl x509 -in localhost.crt -text`