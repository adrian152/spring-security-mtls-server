package net.vrabie.security.demosecurity;

import net.vrabie.security.demosecurity.security.Params;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.InputStream;
import java.net.URL;

@SpringBootApplication
public class DemoSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSecurityApplication.class, args);
    }

    @PostConstruct
    void postConstruct(){
        setTrustStoreParams();
    }


    private static void setTrustStoreParams() {
//        File filePath = new File(Params.trustStorePath);
//        String tsp = filePath.getAbsolutePath();
        URL resource = DemoSecurityApplication.class.getClassLoader().getResource("certs/client.jks");
        String path = resource.getPath();
        System.out.println("Iaka path frmo path:" + path);

//        System.out.println("Iaka path: "+tsp);
        System.setProperty("javax.net.ssl.trustStore", path);
        System.setProperty("javax.net.ssl.trustStorePassword", Params.trustStorePassword);
        System.setProperty("javax.net.ssl.keyStoreType", Params.defaultType);
        System.out.println(System.getProperty("javax.net.ssl.trustStore"));

    }
}
