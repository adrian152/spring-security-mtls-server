package net.vrabie.security.demosecurity.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class EntryPointController {
    @GetMapping("/hello2")
    public String greetings() {
        return "Greetings from Spring-Security";
    }

    @GetMapping("/hello")
    public String greetingsPrincipal(Principal principal) {
        return "Greetings from Spring-Security: "+ principal.getName();
    }
}
