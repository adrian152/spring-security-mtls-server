package net.vrabie.security.demosecurity.security;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


public class AccountUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username)  {
        if (!username.equalsIgnoreCase("my-client")) {
            throw new UsernameNotFoundException("bye bye " + username + "!");
        }
        return new User(username, "whatever", true, true, true, true, AuthorityUtils.createAuthorityList("ROLE_ADMIN", "ROLE_USER"));
    }
}
