package net.vrabie.security.demosecurity.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.HashMap;

public class MyInMemoryDetailsService implements UserDetailsService {

    private HashMap<String, UserDetails> localUsers = new HashMap<>();
    {
        localUsers.put("user", new MyUserDetails("user", "password"));
        localUsers.put("myuser", new MyUserDetails("myuser", "iaka"));
        localUsers.put("adrian", new MyUserDetails("adrian", "vrabie"));
        localUsers.put("my-client", new MyUserDetails("my-client", null));
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // TODO: 12/1/2020 get a UserDetails per username
        return localUsers.get(username);
    }
}
