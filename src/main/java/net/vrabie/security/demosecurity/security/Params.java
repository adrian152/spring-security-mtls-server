package net.vrabie.security.demosecurity.security;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


@Component
@ConfigurationProperties(prefix = "params")
@Data
public class Params {
    public static String trustStorePath = "certs/client.jks";
    public static String trustStorePassword = "private";
//    public static String keyStorePath = "config/wso2carbon.jks";
//    public static String keyStorePassword = "wso2carbon";
    public static String defaultType = "JKS";
}
